<?php

return [
    'user' => [
        'statuses' => [
            'ACTIVE' => 1,
            'INACTIVE' => 2
        ],
        'roles' => [
            'ADMIN' => 1,
            'EDITOR' => 2,
            'TYPICAL' => 3
        ],
        'types' => [
            'SHOPKEEPER' => 1,
            'DISTRIBUTOR' => 2
        ],
    ],
];