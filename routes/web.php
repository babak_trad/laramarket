<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.'], function () {

    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\LoginController@adminLogin')->name('dashboard');
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');

    Route::group(['prefix' => 'users', 'namespace' => 'User', 'as' => 'users.', 'middleware' => 'auth:web_admin'],
        function () {
            Route::get('/list', 'UsersController@list')->name('list');
            Route::get('/create', 'UsersController@create')->name('create');
            Route::post('/store', 'UsersController@store')->name('store');
        });

    Route::group(['prefix' => 'logs/socialApps', 'namespace' => 'Logs\\SocialApps', 'as' => 'logs.socialApps.'],
        function () {
            Route::get('/telegram', 'Telegram\\TelegramErrorController@getUpdates')->name('telegram');
            Route::get('/message', 'Telegram\\TelegramErrorController@createMessage')->name('message');
            Route::post('/send', 'Telegram\\TelegramErrorController@sendMessage')->name('send');
        });

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
