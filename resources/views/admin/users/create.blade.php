@extends('layouts.admin')

@section('wrapper')
    <!--main content start-->
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        <p>
                            ثبت کاربر جدید
                        </p>
                    </header>
                    <div class="panel-body">
                        @include('partials.success')
                        @include('admin.users.form')
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
    <!--main content end-->
@endsection