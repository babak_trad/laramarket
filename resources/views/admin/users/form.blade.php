@if($errors->any())
    <div class="alert alert-danger">
        <strong>خطا: </strong>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <br>
        @foreach($errors->all() as $message)
            {{ $message }}<br>
        @endforeach
    </div>
@endif
<form action="{{ route('admin.users.store') }}" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="inputFullName">نام کامل</label>
            <input type="text" name="FullName" class="form-control" id="inputFullName" placeholder="به فارسی تایپ کنید">
        </div>
        <div class="form-group col-md-6">
            <label for="inputUsername">نام کاربری</label>
            <input type="text" name="Username" class="form-control" id="inputUsername" placeholder="به انگلیسی تایپ کنید">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="inputEmail">ایمیل</label>
            <input type="email" name="Email" class="form-control" id="inputEmail" placeholder="ایمیل معتبر">
        </div>
        <div class="form-group col-md-6">
            <label for="inputMobile">موبایل</label>
            <input type="number" name="Mobile" class="form-control" id="inputMobile" placeholder="موبایل معتبر">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="inputPassword">رمز عبور</label>
            <input type="password" name="Password" class="form-control" id="inputPassword" placeholder="مانند: !example12">
        </div>
        <div class="form-group col-md-6">
            <label for="inputRepeatPassword">تکرار رمز عبور</label>
            <input type="password" name="RepeatPassword" class="form-control" id="inputRepeatPassword" placeholder="رمز عبور خود را تکرار کنید">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="UserRoleSelect">نقش کاربری</label>
            <select class="form-control" name="UserRole" id="UserRoleSelect">
                @foreach($roles as $key => $value)
                    <option value="{{ $value }}">{{ $key }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-6">
            <label for="UserStatusSelect">وضعیت</label>
            <select class="form-control" name="UserStatus" id="UserStatusSelect">
                @foreach($statuses as $key => $value)
                    <option value="{{ $value }}">{{ $key }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-6">
            <label for="UserTypeSelect">نوع کاربری</label>
            <select class="form-control" name="UserType" id="UserTypeSelect">
                @foreach($types as $key => $value)
                    <option value="{{ $value }}">{{ $key }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group col-md-6">
        <button type="submit" class="btn btn-primary" >ثبت کاربر</button>
    </div>
</form>
