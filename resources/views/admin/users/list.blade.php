@extends('layouts.admin')

@section('wrapper')
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Dynamic Table

                    </header>
                    <div id="sample_1_wrapper" class="dataTables_wrapper form-inline" role="grid">
                        <div class="row">
                            <div class="col-sm-6">
                                <div id="sample_1_length" class="dataTables_length"><label><select size="1"
                                                                                                   name="sample_1_length"
                                                                                                   aria-controls="sample_1"
                                                                                                   class="form-control">
                                            <option value="10" selected="selected">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select> records per page</label></div>
                            </div>
                            <div class="col-sm-6">
                                <div class="dataTables_filter" id="sample_1_filter"><label>Search: <input type="text"
                                                                                                          aria-controls="sample_1"
                                                                                                          class="form-control"></label>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped border-top dataTable" id="sample_1"
                               aria-describedby="sample_1_info">
                            <thead>
                            <tr role="row">
                                <th style="width: 13px;" class="sorting_disabled" role="columnheader" rowspan="1"
                                    colspan="1" aria-label="">
                                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes">
                                </th>
                                <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_1"
                                    rowspan="1" colspan="1" aria-label="Username: activate to sort column ascending"
                                    style="width: 179px; ">نام کامل
                                </th>
                                <th class="hidden-phone sorting" role="columnheader" tabindex="0"
                                    aria-controls="sample_1" rowspan="1" colspan="1"
                                    aria-label="Email: activate to sort column ascending" style="width: 338px;">ایمیل
                                </th>
                                <th class="hidden-phone sorting" role="columnheader" tabindex="0"
                                    aria-controls="sample_1" rowspan="1" colspan="1"
                                    aria-label="Points: activate to sort column ascending" style="width: 124px;">موبایل
                                </th>
                                <th class="hidden-phone sorting" role="columnheader" tabindex="0"
                                    aria-controls="sample_1" rowspan="1" colspan="1"
                                    aria-label="Joined: activate to sort column ascending" style="width: 187px;">نقش
                                    کاربری
                                </th>
                                <th class="hidden-phone sorting" role="columnheader" tabindex="0"
                                    aria-controls="sample_1" rowspan="1" colspan="1"
                                    aria-label="Joined: activate to sort column ascending" style="width: 187px;">وضعیت
                                </th>
                                <th class="hidden-phone sorting" role="columnheader" tabindex="0"
                                    aria-controls="sample_1" rowspan="1" colspan="1"
                                    aria-label="Joined: activate to sort column ascending" style="width: 187px;">موجودی
                                </th>
                                <th class="hidden-phone sorting" role="columnheader" tabindex="0"
                                    aria-controls="sample_1" rowspan="1" colspan="1"
                                    aria-label=": activate to sort column ascending" style="width: 195px;">تاریخ عضویت
                                </th>
                            </tr>
                            </thead>

                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                            @foreach($users[0] as $user)
                                    <tr>
                                        <td class="  sorting_1">
                                            <input type="checkbox" class="checkboxes" value="1">
                                        </td>
                                        <td class=" ">
                                            {{ $user[0]['name'] }}
                                        </td>
                                        <td class="hidden-phone ">
                                            <a href="mailto:jhone-doe@gmail.com">
                                                {{ $user[0]['user_email'] }}
                                            </a>
                                        </td>
                                        <td class="hidden-phone ">
                                            {{ $user[0]['user_mobile'] }}
                                        </td>
                                        <td class="center hidden-phone ">
                                            {{ $user[0]['user_role'] }}
                                        </td>
                                        <td class="hidden-phone ">
                                        <span class="label label-{{ $user[0]['user_status'] == 0 ? 'success' : 'danger' }}">
                                            {{ $user[0]['user_status'] == 0 ? 'غیرفعال' : 'فعال' }}
                                        </span>
                                        </td>
                                        <td class="center hidden-phone ">
                                            0
                                            {{--{{ $user[0]['user_wallet_id'] }}--}}
                                        </td>
                                        <td class="center hidden-phone ">
                                            {{ $user[0]['created_at'] }}
                                        </td>
                                    </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="dataTables_info" id="sample_1_info">
                                    Showing 1 to 10 of 25 entries
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="dataTables_paginate paging_bootstrap pagination">
                                    {{--<ul>--}}
                                        {{--<li class="prev disabled">--}}
                                            {{--<a href="#">← Prev</a>--}}
                                        {{--</li>--}}
                                        {{--<li class="active">--}}
                                            {{--<a href="#">1</a>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<a href="#">2</a>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<a href="#">3</a>--}}
                                        {{--</li>--}}
                                        {{--<li class="next">--}}
                                            {{--<a href="#">Next → </a>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                    {{ $users[1] }}
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
@endsection
