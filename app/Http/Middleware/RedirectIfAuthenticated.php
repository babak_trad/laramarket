<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {

            if ('web_admin' === $guard) {
                return redirect()->route('admin.users.list');
            }

            if ('web' === $guard) {
                return redirect()->route('home');
            }

            return redirect()->route('home');
        }

        return $next($request);
    }
}
