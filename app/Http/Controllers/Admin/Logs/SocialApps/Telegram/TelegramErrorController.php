<?php

namespace App\Http\Controllers\Admin\Logs\SocialApps\Telegram;

use App\Services\ErrorReportingService\ToTelegramHandler\TelegramErrorHandler;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramErrorController extends Controller
{
    public function getHome()
    {
        return view('home');
    }

    public function getUpdates()
    {
        $updates = Telegram::getUpdates();
        dd($updates);
    }

    public function createMessage()
    {
        return (new TelegramErrorHandler())->getSendMessage();
    }

    public function sendMessage(Request $request)
    {
        return (new TelegramErrorHandler())->postSendMessage($request);
    }
}
