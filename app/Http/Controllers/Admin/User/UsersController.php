<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Requests\User\UserCreateFormRequest;
use App\Models\Users\User;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Services\User\UserCreatedService;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $repository)
    {
        $this->userRepository = $repository;
    }

    public function list()
    {
        $per_page = 10;
        $users = $this->userRepository->paginate($per_page)->getPaginator();
        return view('admin.users.list', compact('users'));
    }

    public function create()
    {
        $statuses = config('strings.user.statuses');
        $roles = config('strings.user.roles');
        $types = config('strings.user.types');
        return view('admin.users.create', compact('statuses', 'roles', 'types'));
    }

    public function store(UserCreateFormRequest $formRequest)
    {
        $newUserCreated = new UserCreatedService([
            'FullName' => $formRequest->FullName,
            'Email' => $formRequest->Email,
            'Username' => $formRequest->Username,
            'Mobile' => $formRequest->Mobile,
            'UserStatus' => $formRequest->UserStatus,
            'UserRole' => $formRequest->UserRole,
            'UserType' => $formRequest->UserType,
            'Password' => bcrypt($formRequest->Password)
        ]);
        if (!$newUserCreated->apply()) {
            return back()->with('errors', 'عملیات ثبت ناموفق بود!');
        }
        return redirect()->back()->with('success', 'کاربر جدید باموفقیت ثبت شد!');
    }

}
