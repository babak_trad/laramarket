<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'FullName' => 'required|max:255',
            'Email' => 'required|E-Mail',
            'Username' => 'required|max:255'
        ];
    }

    public function messages()
    {
        return [
            'FullName.required' => 'لطفا نام خود را وارد کنید',
            'FullName.max:255' => 'نام وارد شده طولانی است!',
            'Email.required' => 'لطفا ایمیل خود را وارد کنید',
            'Email.E-Mail' => 'عبارت ایمیل وارد شده نامعتبر است',
            'Username.required' => 'لطفا نام کاربری خود را وارد کنید',
            'Username.max:255' => 'نام کاربری وارد شده طولانی است!'
        ];
    }
}
