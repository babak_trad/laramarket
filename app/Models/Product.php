<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = ['product_id', 'product_like_count', 'product_view_count', 'product_share_count'];
}
