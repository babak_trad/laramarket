<?php

namespace App\Models\Users;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    use Notifiable;

    protected $primaryKey = 'admin_id';

    protected $guarded = 'admin_id';

    protected $hidden = ['admin_password', 'remember_token'];

    public function getAuthPassword()
    {
        return $this->admin_password;
    }
}
