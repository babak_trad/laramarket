<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $guarded =['post_id','post_like_count','post_view_count','post_share_count'];
}
