<?php

namespace App\Providers;

use App\Entities\Contracts\EntityInterface;
use App\Entities\User\EloquentUserEntityMapper;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Repositories\Eloquent\User\EloquentUserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserRepositoryInterface::class, EloquentUserRepository::class);
        //$this->app->bind(EntityInterface::class, EloquentUserEntityMapper::class);
    }
}
