<?php

namespace App\Providers;

use App\Entities\User\UserEntityInterface;
use App\Models\Users\Admin;
use App\Models\Users\User;
use App\Policies\AdminPolicy;
use App\Policies\UserPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        UserEntityInterface::class => UserPolicy::class,
        Admin::class => AdminPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::resource('CanCrudUsers', 'App\Policies\UserPolicy');
        Gate::resource('CanCrudAdmins', 'App\Policies\AdminPolicy');
    }
}
