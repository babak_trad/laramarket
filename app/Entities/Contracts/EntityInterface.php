<?php
/**
 * Created by PhpStorm.
 * User: radra
 * Date: 6/9/2019
 * Time: 8:28 PM
 */

namespace App\Entities\Contracts;


interface EntityInterface
{
    public function getEntityArray();

    public function getPagination();

    public function getRelations();

}