<?php
/**
 * Created by PhpStorm.
 * User: radra
 * Date: 6/18/2019
 * Time: 9:38 AM
 */

namespace App\Entities\User;

use App\Repositories\Contracts\defineEntity;

interface UserEntityInterface extends defineEntity
{
    public function getId(): array;

    public function getName(): array;

    public function getUsername(): array;

    public function getPassword(): array;

    public function getEmail(): array;

    public function getMobile(): array;

    public function getRole(): array;

    public function getStatus(): array;

    public function getEmailToken(): array;

    public function getMobileToken(): array;

    public function getType(): array;

    public function getRememberToken(): array;

    public function getCreatedAt(): array;

    public function getUpdatedAt(): array;

    public function getPaginator(): array;
}