<?php
/**
 * Created by PhpStorm.
 * User: radra
 * Date: 6/2/2019
 * Time: 12:53 PM
 */

namespace App\Entities\User;

class EloquentUserEntityMapper implements UserEntityInterface
{
    private $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function getId(): array
    {
        return $this->user->pluck('user_id')->all();
    }

    public function getName(): array
    {
        return $this->user->pluck('name')->all();
    }

    public function getUsername(): array
    {
        return $this->user->pluck('username')->all();
    }

    public function getPassword(): array
    {
        return $this->user->pluck('user_password')->all();
    }

    public function getEmail(): array
    {
        return $this->user->pluck('user_email')->all();
    }

    public function getMobile(): array
    {
        return $this->user->pluck('user_mobile')->all();
    }

    public function getRole(): array
    {
        return $this->user->pluck('user_role')->all();
    }

    public function getStatus(): array
    {
        return $this->user->pluck('user_status')->all();
    }

    public function getEmailToken(): array
    {
        return $this->user->pluck('user_email_token')->all();
    }

    public function getMobileToken(): array
    {
        return $this->user->pluck('user_mobile_token')->all();
    }

    public function getType(): array
    {
        return $this->user->pluck('user_type')->all();
    }

    public function getRememberToken(): array
    {
        return $this->user->pluck('remember_token')->all();
    }

    public function getCreatedAt(): array
    {
        return $this->user->pluck('created_at')->all();
    }

    public function getUpdatedAt(): array
    {
        return $this->user->pluck('updated_at')->all();
    }

    public function getPaginator(): array
    {
        $entity = [];
        for ($i = 0; $i < count($this->user); $i++) {
            $entity[$i] = array(
                [
                    'name' => $this->getName()[$i],
                    'username' => $this->getUsername()[$i],
                    'user_email' => $this->getEmail()[$i],
                    'user_password' => $this->getPassword()[$i],
                    'user_mobile' => $this->getMobile()[$i],
                    'user_role' => $this->getRole()[$i],
                    'user_status' => $this->getStatus()[$i],
                    //'user_email_token' => $this->getEmailToken()[$i],
                    //'user_mobile_token' => $this->getMobileToken()[$i],
                    'remember_token' => $this->getRememberToken()[$i],
                    'created_at' => $this->getCreatedAt()[$i],
                    'updated_at' => $this->getUpdatedAt()[$i],
                ]
            );
        }

        $links = $this->user->fragment('foo')->links();

        return [$entity, $links];

    }
}