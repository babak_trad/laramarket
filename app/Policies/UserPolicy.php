<?php

namespace App\Policies;

use App\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * Create a new policy instance.
     *
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function view()
    {
        return $this->userRepository->find(Auth::id())->getStatus()[0] == 'ADMIN';
    }
}
