<?php
/**
 * Created by PhpStorm.
 * User: radra
 * Date: 7/17/2019
 * Time: 2:31 PM
 */

namespace App\Services\User;


use App\Entities\User\UserEntityInterface;
use App\Repositories\Contracts\UserRepositoryInterface;

class UserCreatedService
{
    /**
     * @var array
     */
    private $userData;
    private $userRepository;

    /**
     * UserCreatedService constructor.
     */
    public function __construct(array $userData)
    {
        $this->userData = $userData;
        $this->userRepository = resolve(UserRepositoryInterface::class);
    }

    public function apply()
    {
        $newUser = $this->userRepository->store([
            'name' => $this->userData['FullName'],
            'user_email' => $this->userData['Email'],
            'username' => $this->userData['Username'],
            'user_mobile' => $this->userData['Mobile'],
            'user_status' => $this->userData['UserStatus'],
            'user_role' => $this->userData['UserRole'],
            'user_type' => $this->userData['UserType'],
            'user_password' => $this->userData['Password']
        ]);

        if ($newUser instanceof UserEntityInterface) {
            // TODO : send notification
            return true;
        }
        return false;
    }
}