<?php
/**
 * Created by PhpStorm.
 * User: radra
 * Date: 6/17/2019
 * Time: 2:16 AM
 */

namespace App\Services\ErrorReportingService\ToTelegramHandler;

use Illuminate\Support\Facades\Validator;
use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramErrorHandler
{
    public function getSendMessage()
    {
        return view('admin.logs.type-message');
    }

    public function postSendMessage($request)
    {
        $rules = [
            'message' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails())
        {
            return redirect()->back()
                ->with('status', 'danger')
                ->with('message', 'Message is required');
        }

        Telegram::sendMessage([
            'chat_id' => env('GROUP_ID'),
            'text' => $request->get('message')
        ]);

        return redirect()->back()
            ->with('status', 'success')
            ->with('message', 'Message sent');
    }
}