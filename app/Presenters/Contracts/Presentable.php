<?php
/**
 * Created by PhpStorm.
 * User: radra
 * Date: 6/20/2019
 * Time: 5:30 AM
 */

namespace App\Presenters\Contracts;


use App\Presenters\Exceptions\UserPresenterNotFoundException;

trait Presentable
{
    protected $presenterInstance;

    public function present()
    {
        if (!$this->presenter || !class_exists($this->presenter)) {
            throw new UserPresenterNotFoundException("Presenter Not Found!");
        }
        if (!$this->presenterInstance) {
            $this->presenterInstance = new $this->presenter($this);
        }
        return $this->presenterInstance;
    }

}