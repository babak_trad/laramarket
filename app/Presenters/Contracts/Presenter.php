<?php
/**
 * Created by PhpStorm.
 * User: radra
 * Date: 6/20/2019
 * Time: 5:12 AM
 */

namespace App\Presenters\Contracts;


abstract class Presenter
{
    protected $entity;

    public function __construct($entity)
    {
        $this->entity = $entity;
    }

    public function __get($property)
    {
        if (method_exists($this, $property)) {
            return $this->{$property}();
        }
        return $this->entity->{$property};
    }
}