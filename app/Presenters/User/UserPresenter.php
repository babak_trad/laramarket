<?php
/**
 * Created by PhpStorm.
 * User: radra
 * Date: 6/20/2019
 * Time: 5:21 AM
 */

namespace App\Presenters\User;


use App\Presenters\Contracts\Presenter;

class UserPresenter extends Presenter
{
    public function role()
    {
        switch ($this->entity->user_role) {
            case 1:
                return "ادمین";
                break;
            case 2:
                return "نویسنده";
                break;
            case 3:
                return "کاربر";
                break;
            default:
                return "تاییده نشده";
        }
    }

    public function status()
    {
        if ($this->entity->user_status == 0) {
            return "غیرفعال";
        }
        return "فعال";
    }

    public function type()
    {
        if ($this->entity->user_type) {
            return "مغازه دار";
        }
        return "توزیع کننده";
    }
}