<?php
/**
 * Created by PhpStorm.
 * User: radra
 * Date: 6/3/2019
 * Time: 3:32 AM
 */

namespace App\Repositories\Eloquent\User;

use App\Entities\User\UserEntityInterface;
use App\Entities\User\EloquentUserEntityMapper;
use App\Models\Users\User;
use App\Repositories\Contracts\defineEntity;
use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;

class EloquentUserRepository implements RepositoryInterface, UserRepositoryInterface
{
    protected $model = User::class;

    public function all(array $columns = null, array $relations = []): defineEntity
    {
        if (!is_null($columns)) {
            return new EloquentUserEntityMapper($this->model::get($columns));
        }
        if (!empty($relations)) {
            return new EloquentUserEntityMapper($this->model::with($relations));
        }
        return new EloquentUserEntityMapper($this->model::all());
    }

    public function find(int $ID): defineEntity
    {
        $user = $this->model::where('user_id', $ID);
        return new EloquentUserEntityMapper($user->get());
    }

    public function findBy(array $criteria, array $columns = null, bool $single = false): defineEntity
    {
        $user = null;
        foreach ($criteria as [$key, $condition, $value]) {
            $user = $this->model::where($key, $condition, $value);
        }
        $method = $single ? 'first' : 'get';

        return !is_null($columns) ? new EloquentUserEntityMapper($user->{$method}($columns)) :
            new EloquentUserEntityMapper($user->{$method}());
    }

    public function store(array $item): defineEntity
    {
        return new EloquentUserEntityMapper($this->model::create($item));
    }

    public function storeMany(array $items): defineEntity
    {
        return new EloquentUserEntityMapper($this->model::createMany($items));
    }

    public function update(int $ID, array $item): defineEntity
    {
        $user = $this->model::find($ID);
        return new EloquentUserEntityMapper($this->model::update($user));
    }

    public function updateBy(array $criteria, array $data): defineEntity
    {
        $users = null;
        foreach ($criteria as $key => $value) {
            $users = new EloquentUserEntityMapper($this->model::where($key, $value));
        }
        return $users;
    }

    public function delete(int $ID): defineEntity
    {
        return new EloquentUserEntityMapper($this->model::destroy($ID));
    }

    public function deleteBy(array $criteria): defineEntity
    {
        $users = null;

        foreach ($criteria as $key => $value) {
            $users = new EloquentUserEntityMapper($this->model::where($key, $value)->delete());
        }
        return $users;
    }

    public function paginate(int $per_page = 20): defineEntity
    {
        return new EloquentUserEntityMapper($this->model::paginate($per_page));
        //return $this->model::paginate($per_page);
    }
}