<?php
/**
 * Created by PhpStorm.
 * User: radra
 * Date: 6/2/2019
 * Time: 1:18 PM
 */

namespace App\Repositories\Contracts;

class EloquentBaseRepository implements RepositoryInterface
{

    public function all(array $columns = null, array $relations = [])
    {
        // TODO: Implement all() method.
    }

    public function paginate(array $columns = null, int $per_page = 50)
    {
        // TODO: Implement paginate() method.
    }

    public function find(int $ID, array $columns = null)
    {
        // TODO: Implement find() method.
    }

    public function findBy(array $criteria, array $columns = null, bool $single = false)
    {
        // TODO: Implement findBy() method.
    }

    public function store(array $item)
    {
        // TODO: Implement store() method.
    }

    public function storeMany(array $items)
    {
        // TODO: Implement storeMany() method.
    }

    public function update(int $ID, array $item)
    {
        // TODO: Implement update() method.
    }

    public function updateBy(array $criteria, array $data)
    {
        // TODO: Implement updateBy() method.
    }

    public function delete(int $ID)
    {
        // TODO: Implement delete() method.
    }

    public function deleteBy(array $criteria)
    {
        // TODO: Implement deleteBy() method.
    }
}