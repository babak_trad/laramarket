<?php
/**
 * Created by PhpStorm.
 * User: radra
 * Date: 6/2/2019
 * Time: 12:57 PM
 */

namespace App\Repositories\Contracts;

interface RepositoryInterface
{
    public function all(array $columns = null, array $relations = []): defineEntity;

    public function paginate(int $per_page): defineEntity;

    public function find(int $ID): defineEntity;

    public function findBy(array $criteria, array $columns = null, bool $single = false): defineEntity;

    public function store(array $item): defineEntity;

    public function storeMany(array $items): defineEntity;

    public function update(int $ID, array $item): defineEntity;

    public function updateBy(array $criteria, array $data): defineEntity;

    public function delete(int $ID): defineEntity;

    public function deleteBy(array $criteria): defineEntity;

}