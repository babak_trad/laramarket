<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Users\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'user_email' => $faker->unique()->safeEmail,
        'user_password' => bcrypt('babakt'), // secret
        'remember_token' => str_random(10),
        'user_email_token' => str_random(10),
        'user_mobile_token' => str_random(10),
    ];
});
