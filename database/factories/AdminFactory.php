<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Users\Admin::class, function (Faker $faker) {
    return [
        'admin_name' => $faker->name,
        'admin_email' => $faker->unique()->safeEmail,
        'admin_password' => bcrypt('admin'.str_random(2)),
        'remember_token' => str_random(10),
    ];
});
