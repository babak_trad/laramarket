<?php

use App\Models\Users\Admin;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'admin_name' => 'Babak',
            'admin_email' => 'babakt@gmail.com',
            'admin_password' => bcrypt('babak8612'),
        ]);
    }
}
