<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('admin_id');
            $table->string('admin_name');
            $table->string('admin_username')->default('admin'.mt_rand(1,9));
            $table->string('admin_email')->unique();
            $table->string('admin_password');
            $table->string('admin_mobile', 11)->nullable();
            $table->tinyInteger('admin_status')->default(0);
            $table->string('admin_email_token')->default("email".str_random(10));
            $table->string('admin_mobile_token')->default("mobile".str_random(10));
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
