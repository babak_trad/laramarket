<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('name');
            $table->string('username')->default('babakt');
            $table->string('user_email')->unique();
            $table->string('user_password');
            $table->string('user_mobile', 11)->nullable();
            $table->tinyInteger('user_role')->default(1);
            $table->tinyInteger('user_status')->default(0);
            $table->string('user_email_token')->default("email".str_random(10));
            $table->string('user_mobile_token')->default("mobile".str_random(10));
            $table->integer('user_wallet_id')->default(0);
            $table->tinyInteger('user_type')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
